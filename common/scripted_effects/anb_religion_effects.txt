﻿mark_faiths_to_found = {
	faith:regent_court = { 
		set_variable = { name = to_found }
		set_variable = { name = block_conversion_till_decision_taken }
	}
}

found_faith_no_conversion = {
	$FAITH$ = {
		remove_variable = to_found
		custom_tooltip = refound_faith_tooltip
	}
}