﻿
# Enhance Ability Spell #
#V1 - WIP
anb_enhance_ability_spell = {
	uses_agents = no
	skill = learning
	resistance_per_skill_point = 0
	spymaster_power_per_skill_point = 0
	spymaster_resistance_per_skill_point = 0
	power_per_skill_point = 8
	uses_resistance = no
	minimum_success = 5
	maximum_success = 100
	minimum_progress_chance = 10
	
	allow = {
		is_adult = yes
		scope:target = {
			is_adult = yes
		}

		#Cannot cast if someone is already casting the same one on the target
		custom_description = {
			text = anb_spell_already_being_cast_on
			subject = scope:target
			scope:target = {
				NOT = {
					any_targeting_scheme = {
						scheme_type = anb_enhance_ability_spell
					}
				}
			}
		}

		#Cannot cast if already been cast on the character in last 3 years
		NOT = { scope:target = { has_character_flag = recently_had_enhance_ability_attempt } }

		#Add cannot cast if someone already has an enhanced stat?

		#add 'knows spell' trigger later
	}

	valid = {
		NOT = { is_at_war_with = scope:target }
		scope:target = {
			OR = {
				exists = location
				in_diplomatic_range = scope:owner
			}
		}
	}

	base_success_chance = {
		#Add more base success chance modifiers here later
		#All WIP

		base = 0

		# CASTER/ACTOR #
		#Number of perks in magic lifestyle
		# Wiki lies - these don't work
		# modifier = {
		# 	add = scope:owner.learning_lifestyle_perks
		# }
		
		#Magical affinity level
		modifier = {
			add = 20
			scope:owner = {
				has_trait = magical_affinity_2
			}
		}
		modifier = {
			add = 40
			scope:owner = {
				has_trait = magical_affinity_3
			}
		}


		# RECIPIENT #

		#Intelligent already
		modifier = {
			add = 20
			scope:owner = {
				has_trait = intellect_good_1
			}
		}
		modifier = {
			add = 25
			scope:owner = {
				has_trait = intellect_good_2
			}
		}
		modifier = {
			add = 30
			scope:owner = {
				has_trait = intellect_good_3
			}
		}

		#A dumbass
		modifier = {
			add = -20
			scope:owner = {
				has_trait = intellect_bad_1
			}
		}
		modifier = {
			add = -25
			scope:owner = {
				has_trait = intellect_bad_2
			}
		}
		modifier = {
			add = -30
			scope:owner = {
				has_trait = intellect_bad_3
			}
		}
	}

	on_ready = {
		scheme_owner = {
			trigger_event =  anb_enhance_ability_outcome.1
		}
	}

	on_invalidated = {
		scope:target = {
			remove_character_flag = enhance_ability_diplomacy
			remove_character_flag = enhance_ability_martial
			remove_character_flag = enhance_ability_stewardship
			remove_character_flag = enhance_ability_intrigue
			remove_character_flag = enhance_ability_learning
			remove_character_flag = enhance_ability_prowess
		}
		if = {
			limit = { scope:target = { is_alive = no } }
			scope:owner = { trigger_event = anb_spell_interruption.3 }
		}
		else_if = {
			limit = { NOT = { scope:target = { in_diplomatic_range = scope:owner } } }
			scope:owner = { trigger_event = anb_spell_interruption.1 }
			scope:target = { trigger_event = anb_spell_interruption.101 }
		}
		else_if = {
			limit = { scope:owner = { is_at_war_with = scope:target } }
			scope:owner = { trigger_event = anb_spell_interruption.6 }
			scope:target = { trigger_event = anb_spell_interruption.106 }
		}
	}

	on_monthly = {
		scheme_owner = {
			trigger_event = {
				on_action = anb_enhance_ability_ongoing
				days = { 1 7 }
			}
		}
	}
	
	success_desc = anb_enhance_ability_success
}
